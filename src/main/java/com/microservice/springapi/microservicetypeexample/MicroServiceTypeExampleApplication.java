package com.microservice.springapi.microservicetypeexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroServiceTypeExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(MicroServiceTypeExampleApplication.class, args);
    }

}
